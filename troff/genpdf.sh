#!/bin/sh
[ -r ~/heirloom/heirloom.sh ] && . ~/heirloom/heirloom.sh
soelim $1 | tbl | troff -Tps | dpost | ps2pdf - > $2
